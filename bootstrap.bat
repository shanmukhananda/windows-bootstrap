@echo off

setlocal

set pkg_file=%~1

if "%pkg_file%"=="" (
	echo Provide package file
	exit /b 1
)

set choco_option=%~2
if "%choco_option%"=="" (
    set choco_option=install
)

net session >nul 2>&1
if %errorLevel% == 0 (
	echo Success: Administrative permissions confirmed
) else (
	echo Failure: Current permissions inadequate
	exit /b 1
)

@"%SystemRoot%\System32\WindowsPowerShell\v1.0\powershell.exe" -NoProfile -ExecutionPolicy Bypass -Command "iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))"
set PATH=%PATH%;%ALLUSERSPROFILE%\chocolatey\bin

choco feature enable -n=allowGlobalConfirmation
choco upgrade chocolatey

for /F "tokens=*" %%A in (%pkg_file%) do (
   choco "%choco_option%" %%A
)

choco feature disable -n=allowGlobalConfirmation

echo User should install below packages:
echo Python from VisualStudioInstaller
echo Anaconda from VisualStudioInstaller
echo Clover from internet

endlocal